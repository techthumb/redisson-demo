package com.nasdaq.redisson.demo;

import org.redisson.api.StreamId;

import java.util.Arrays;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class IServiceImpl implements IService {
    @Override
    public long add(long... numbers) {
        System.out.println("adding numbers = [" + csv(numbers) + "]");
        return Arrays.stream(numbers).sum();
    }

    private String csv(long... numbers) {
        StringBuilder foo = new StringBuilder();
        Arrays.stream(numbers).forEach(foo::append);
        return foo.toString();
    }
}
