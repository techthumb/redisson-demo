package com.nasdaq.redisson.demo;

public class ImplMain {
    public static void main(String[] args) {
        new RedissonClientSupplier().get().getRemoteService().register(IService.class, new IServiceImpl());
    }
}
