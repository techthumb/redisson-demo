package com.nasdaq.redisson.demo;

public class WebappMain {

    public static void main(String[] args) {
        IService iService = new RedissonClientSupplier().get().getRemoteService().get(IService.class);
        System.out.println(iService.add(1, 2, 3));
    }
}
