package com.nasdaq.redisson.demo;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import java.util.function.Supplier;

public class RedissonClientSupplier implements Supplier<RedissonClient> {

    private final RedissonClient redissonClient;

    public RedissonClientSupplier() {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://localhost:6379");
        redissonClient = Redisson.create(config);
    }

    @Override
    public RedissonClient get() {
        return redissonClient;
    }
}
