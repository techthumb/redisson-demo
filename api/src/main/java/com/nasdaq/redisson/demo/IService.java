package com.nasdaq.redisson.demo;

public interface IService {
    long add(long... numbers);
}
